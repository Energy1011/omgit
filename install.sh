#!/bin/bash
# GPL v3. Copyright (C) 2019 (energy1011[4t)gmail(d0t]com)
#This script installs omgit-fzf inside a project folder and check dependencies
INSTALL_PATH="/usr/local/bin/"
PROGRAM_NAME="OMGit-fzf"
PROGRAM_FILENAME="omgit"

echo "[i] Installer info";
echo "Program name: $PROGRAM_NAME";
echo "Install path: $INSTALL_PATH";
echo "---------------------------";
echo ""

# Check run as root
if [[ $EUID -ne 0 ]]; then
    echo "[i] This installer script must be run as root";
    echo "[i] run like: sudo ./install.sh";
    exit 1
fi

#Check for python 3
python3 --version >& /dev/null
if [ $? -ne 0 ]; then
    echo "[$PROGRAM_NAME]: To use $PROGRAM_NAME, you need python3 installed in your system.";
fi

#copy PROGRAM_FILENAME to local bin
cp omgit "$INSTALL_PATH$PROGRAM_FILENAME";
if [ $? -ne 0 ]; then
    echo "[$PROGRAM_NAME]: Error copying $PROGRAM_NAME to $INSTALL_PATH$PROGRAM_FILENAME"
fi

#Add exec permissions by chmod
chmod +x "$INSTALL_PATH$PROGRAM_FILENAME";
if [ $? -ne 0 ]; then
    echo "[$PROGRAM_NAME]: Error chmod +x to $PROGRAM_FILENAME in $INSTALL_PATH$PROGRAM_FILENAME";
fi

echo "[$PROGRAM_NAME]: Installed OK, enjoy.";