![PScaff](https://gitlab.com/Energy1011/omgit/raw/master/omgit-fzf.png)

## OMGit-fzf
This script is used with fzf to manage git status standar output with fzf, giving more speed using git in the terminal.

## Installation
Clone the project:
```bash
git clone https://gitlab.com/Energy1011/omgit.git
```
Enter the omgit folder:
```bash
cd omgit
```
Once the project is cloned, run:
```bash
sudo ./install.sh
```

## Usage
Once installed, type 'omgit' inside a git project folder.

## STDIN
We can call like this: 
```bash
$ cat list_of_files.txt | omgit
```

Enjoy.


 More information at:
 
 - [Video](https://www.youtube.com/watch?v=wk2vN3zcLH8)
 
 - [Monster penguin blog](https://energy1011.gitlab.io/monsterpenguin/Oh-My-Git,-OMGit-Fzf-un-script-para-aumentar-la-productividad-con-git-en-la-terminal)
